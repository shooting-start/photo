const addResourcesToCache = async (resources) => {
  const cache = await caches.open('v1');
  await cache.addAll(resources);
};

const putInCache = async (request, response) => {
  const cache = await caches.open('v1');
  try {
    await cache.put(request, response);
  } catch(error) {
    console.log(error);
  }
};

const cacheFirst = async (event) => {
  try {
    // 尝试从缓存中获取数据
    const responseFromCache = await caches.match(event.request);
    if (responseFromCache) {
      // 如果缓存中有数据，则返回缓存的数据
      return responseFromCache;
    } else {
      // 如果缓存中没有数据，则从网络获取
      const responseFromNetwork = await fetch(event.request);
      // 将网络响应克隆后放入缓存
      putInCache(event.request, responseFromNetwork.clone())
      // 返回网络响应
      return responseFromNetwork;
    }
  } catch (error) {
    // 如果发生错误，则返回一个错误响应
    return new Response('Error: ' + error.message, { status: 500 });
  }
};


self.addEventListener("install", (event) => {
  self.skipWaiting()
});


self.addEventListener('fetch', async (event) => {
  event.respondWith(cacheFirst(event));
});

